[comment]: <> (Inicio de Acceso a Wiki)

## Acceso a Wiki
Para acceder a la Wiki has click en el botón _Wiki_ de la barra lateral de navegación izquierda, o en este [**link**](https://bitbucket.org/KaryRs/blink_wiki/wiki/Home).
    
[comment]: <> (Fin de Acceso a Wiki)

